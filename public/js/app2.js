var currentTab = 0;
showTab(currentTab);

function showTab(n) {
    let x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    if (n == 0) {
        document.getElementById("prevBtn").style.display = "none";
    } else {
        document.getElementById("prevBtn").style.display = "inline";
    }
    if (n == (x.length - 1)) {
        document.getElementById("nextBtn").innerHTML = "Sign Up";
    } else {
        document.getElementById("nextBtn").innerHTML = "Next";
    }
    fixStepIndicator(n)
}

function nextPrev(n) {
    let x = document.getElementsByClassName("tab");
    if (n == 1 && !validateForm()) return false;
    x[currentTab].style.display = "none";
    currentTab = currentTab + n;
    if (currentTab >= x.length) {
        document.getElementById("regForm").submit();
        return false;
    }
    showTab(currentTab);
}

function validateForm() {
    let x, y, z, i, valid = true;
    x = document.getElementsByClassName("tab");
    y = x[currentTab].getElementsByClassName("form-control");
    z = x[currentTab].getElementsByClassName('error-msg');
    for (i = 0; i < y.length; i++) {
        if (y[i].value == "") {
            z[i].style.display = 'block';
            // and set the current valid status to false:
            valid = false;
        }
    }

    if (valid) {
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid;
}

function fixStepIndicator(n) {
    let i, x = document.getElementsByClassName("step");
    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    x[n].className += " active";
}

let country = document.getElementById('country');
document.addEventListener('DOMContentLoaded', loadCountry);

function loadCountry() {
    let xhr = new XMLHttpRequest();
    //console.log(xhr);
    xhr.open('GET', 'js/country.json', true);

    xhr.onload = function () {
        if(this.status == 200) {
            let listCountry = JSON.parse(this.responseText);

            for (let i = 0; i < listCountry.length; i++) {
                let listCountries = listCountry[i].name;
                let option = document.createElement('option');
                let optionText = document.createTextNode(listCountries);
                option.appendChild(optionText);
                country.appendChild(option);
            }
        }
    }
    xhr.send();
}

function loadPaymentMethod() {
    let payment = document.getElementById('pay-method').value;
    let debitcard = document.getElementById('debit-card');
    let mobilemoney = document.getElementById('mobile-money');
    let healthwallet = document.getElementById('health-wallet');

    if (payment == 'debit-card') {
        debitcard.style.display = 'block';
        mobilemoney.style.display = 'none';
        healthwallet.style.display = 'none';
    } else if (payment == 'mobile-money') {
        mobilemoney.style.display = 'block';
        debitcard.style.display = 'none';
        healthwallet.style.display = 'none';
    } else if (payment == 'health-wallet') {
        healthwallet.style.display = 'block';
        debitcard.style.display = 'none';
        mobilemoney.style.display = 'none';
    } else {
        mobilemoney.style.display = 'none';
        debitcard.style.display = 'none';
        healthwallet.style.display = 'none';
    }
}

// Vanilla Javascript
var input = document.querySelector("#telephone");
window.intlTelInput(input,({
    // options here
}));

$('[data-toggle="datepicker"]').datepicker({
    autoHide: true
});
