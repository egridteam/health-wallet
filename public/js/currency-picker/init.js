(function ($) {
    $(".currency-picker").each(function () {
        let currency = $(this).data().value;
        if (currency) {
            $(this).bfhcurrencies({currency: currency});
        } else {
            $(this).bfhcurrencies({currency: 'USD'});
        }
    });

    $(document).on("change", function () {
        $(".currency-picker").each(function () {
            $(this).bfhcurrencies({currency: 'USD'});
        });
    })

})(jQuery);
