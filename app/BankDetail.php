<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetail extends Model
{
    protected $table = 'bank_details';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
