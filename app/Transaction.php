<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Services\CurrencyService;
use Carbon\Carbon;

/**
 * @property mixed transaction_date
 * @property mixed transaction_currency
 * @property mixed currency
 */
class Transaction extends Model
{
    protected $table = 'transactions';

    public function transactionDate()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->transaction_date)->diffForHumans();
    }

    public function transactionCurrency()
    {
        return CurrencyService::GetCurrencySymbol($this->transaction_currency);
    }

    public function currency()
    {
        return CurrencyService::GetCurrencySymbol($this->currency);
    }
}
