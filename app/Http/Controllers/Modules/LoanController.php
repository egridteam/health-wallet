<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Services\BankService;
use App\Http\Controllers\Services\CurrencyService;
use App\Http\Controllers\Services\LoanService;
use App\Http\Controllers\Services\TransactionService;
use App\Http\Controllers\Utilities\TransactionUtility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class LoanController extends Controller
{
    public function index(Request $request)
    {
        $tdata['user_id']        = auth()->user()->id;

        $bdata['user_id']        = auth()->user()->id;
        $bdata['record_id']      = '';

        $transactions = LoanService::GetLoanTransactions($tdata);

        return view('transactions.loan')->with([
            'transactions'  => $transactions
        ]);
    }

    public function TakeALoan(Request $request)
    {
        $this->validate($request, [
            'amount'        =>  'required|numeric',
        ]);

        $data['user_id']                = Auth::user()->id;
        $data['transaction_amount']     = $request->amount;
        $data['transaction_currency']   = CurrencyService::UNIT_CURRENCY;
        $data['transaction_channel']    = BankService::HEALTH_WALLET;
        $data['transaction_code']       = TransactionUtility::GenerateToken();
        $data['transaction_date']       = Carbon::now();
        $data['transaction_type']       = LoanService::TRANSACTION_TYPE;
        $data['activity']               = LoanService::TRANSACTION_ACTIVITY;
        $data['currency']               = CurrencyService::UNIT_CURRENCY;
        $data['reason']                 = "";

        $response =  TransactionService::LoanTransaction($data);

        // return $response;
        if($response['status'])
            session()->flash('success', $response['reason'] . ' Transaction code:' . $response['transaction_code']);
        else
            session()->flash('error', $response['reason']);

        return redirect('/transactions')->with($response);
    }
}