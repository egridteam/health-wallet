<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Services\BankService;
use App\Http\Controllers\Services\CurrencyService;
use Illuminate\Http\Request;
use Auth;

class PayBillController extends Controller
{
    public function index(Request $request)
    {
        $tdata['user_id'] = Auth::user()->id;
        $currencies = CurrencyService::GetAvaliableCurrencies();
        $mobilebanks = BankService::GetMyMobileServiceDetails($tdata);

        return view('transactions.pay-bill', compact('currencies', 'mobilebanks'));
    }
}