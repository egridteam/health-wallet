<?php

namespace App\Http\Controllers\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Services\BankService;
use App\Http\Controllers\Controller;
use Auth;

class MyBankController extends Controller 
{
    public function Index(Request $request)
    {
        $data['user_id']   = Auth::user()->id;
        $data['record_id'] = $request->id;

        $response = BankService::GetMyBankDetails($data);

        return view('transactions.bank-detail')->with([ 'banks' => $response ]);
    }

    public function AddBankDetail(Request $request)
    {
        $data = []; 

        $request->validate([
            'account_number' => 'required|min:7',
            'account_name'   => 'required',
            'bank_name'      => 'required',
            'bank_type'      => 'required'
        ]);

        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $response = BankService::AddBankMomoDetails($data);

        if($response['status'])
            session()->flash('success', $response['reason']);
        else
            session()->flash('error', $response['reason']);

        return redirect()->back()->with($response);
    }
}