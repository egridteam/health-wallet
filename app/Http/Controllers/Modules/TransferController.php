<?php

namespace App\Http\Controllers\Modules;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Services\BankService;
use App\Http\Controllers\Services\CurrencyService;
use App\Http\Controllers\Services\TransactionService;
use App\Http\Controllers\Services\TransferService;
use App\Http\Controllers\Utilities\TransactionUtility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class TransferController extends Controller
{
    public function index()
    {
        $currencies = CurrencyService::GetAvaliableCurrencies();
        return view('transactions.transfer', compact('currencies'));
    }

    public function MakeATransfer(Request $request)
    {
        $this->validate($request, [
            'phone_number'  =>  'required|integer|min:10',
            'amount'        =>  'required|numeric',
        ]);

        $data['user_id'] = Auth::user()->id;
        $data['phone_number'] = $request->phone_number;

        $receiverInfo = TransferService::GetReceiverIdByPhoneNumber($data);
        $receiver_id = $receiverInfo->id;

        if ($receiverInfo) {
            $data['transaction_amount']     = $request->amount;
            $data['transaction_currency']   = CurrencyService::UNIT_CURRENCY;
            $data['transaction_channel']    = BankService::HEALTH_WALLET;
            $data['transaction_code']       = TransactionUtility::GenerateToken();
            $data['transaction_date']       = Carbon::now();
            $data['transaction_type']       = TransferService::TRANSACTION_TYPE;
            $data['activity']               = TransferService::TRANSACTION_ACTIVITY;
            $data['currency']               = CurrencyService::UNIT_CURRENCY;
            $data['reason']                 = "";
        } else {
            return redirect()->back()->with('message', 'couldn\'t find anyone with such record');
        }

        $response =  TransactionService::TransferTransaction($receiver_id, $data);

        // return $response;
        if($response['status'])
            session()->flash('success', $response['reason'] . ' Transaction code:' . $response['transaction_code']);
        else
            session()->flash('error', $response['reason']);

        return redirect('/transactions')->with($response);
    }
}