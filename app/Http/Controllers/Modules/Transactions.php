<?php
namespace App\Http\Controllers\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Services\BankService;
use App\Http\Controllers\Services\TransactionService;
use App\Http\Controllers\Services\CurrencyService;
use App\Http\Controllers\Utilities\TransactionUtility;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Auth;

class Transactions extends Controller 
{
    public function Index(Request $request)
    {
        $tdata['user_id']        = Auth::user()->id;
        $tdata['record_id']      = $request->id;

        $bdata['user_id']        = Auth::user()->id;
        $bdata['record_id']      = '';

        $transactions = TransactionService::GetTransactions($tdata);
        $currencies = CurrencyService::GetAvaliableCurrencies();
        $banks = BankService::GetMyBankDetails($bdata);

        return view('transactions.index')->with([
            'banks'         => $banks, 
            'currencies'    => $currencies,
            'transactions'  => $transactions
        ]);
    }
    
    public function MakeTopUp(Request $request)
    {
        $this->validate($request, [
            'sending_currency'  =>  'required|integer|min:1',
            'amount'            =>  'required|numeric|min:5',
            'source'            =>  'required|integer|min:1'
        ]);

        $data['user_id'] = Auth::user()->id;
        /**
         * Check for Ghana cedis | else convert (payload) currency to Ghana cedi
        */
        if(CurrencyService::IsGhanaCedis($request->sending_currency)) {
            $data['transaction_amount']     = $request->amount;
            $data['transaction_currency']   = $request->sending_currency;
        } else {
            $convert_object = CurrencyService::ConvertToLocal($request->amount, $request->sending_currency);

            $data['transaction_amount']     = $convert_object['transaction_amount'];
            $data['transaction_currency']   = $convert_object['transaction_currency'];
        }

        $data['transaction_code'] = TransactionUtility::GenerateToken();
        $data['transaction_date'] = Carbon::now();

        $bdata['user_id']        = Auth::user()->id;
        $bdata['record_id']      = $request->source;
        $bank_record = BankService::GetMyBank($bdata);

        if(isset($bank_record)) {
            $data['transaction_channel'] = $bank_record->bank_type;
        }

        $data['transaction_type'] = TransactionService::TRANSACTION_TYPE;
        $data['activity'] = TransactionService::TRANSACTION_ACTIVITY;
        $data['reason'] = "";

        $response =  TransactionService::TopUpTransaction($data);

        // return $response;
        if($response['status'])
            session()->flash('success', $response['reason'] . ' Transaction code:' . $response['transaction_code']);
        else
            session()->flash('error', $response['reason']);

        return redirect()->back()->with($response);
    }
}

