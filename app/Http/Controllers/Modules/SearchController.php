<?php

namespace App\Http\Controllers\Modules;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Services\BankService;
use App\Http\Controllers\Services\CurrencyService;
use App\Transaction;
use Illuminate\Http\Request;
use Auth;


class SearchController extends Controller
{
    public function search(Request $request)
    {
        $bdata['user_id']        = Auth::user()->id;
        $bdata['record_id']      = '';

        $search = $request->input('search');
        $currencies = CurrencyService::GetAvaliableCurrencies();

        $banks = BankService::GetMyBankDetails($bdata);

        $transactions = Transaction::where('transaction_channel', 'LIKE', '%'.$search.'%')
            ->where('user_id', auth()->user()->id)->paginate(5);

        return view('transactions.index')->with([
            'transactions'  => $transactions,
            'currencies'    => $currencies,
            'banks' => $banks
        ]);
    }
}