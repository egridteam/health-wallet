<?php

namespace App\Http\Controllers\Services;

use App\User;

class TransferService
{
    const TRANSACTION_TYPE = 'Debit';
    const TRANSACTION_ACTIVITY = 'Transfer';

    public static function GetReceiverIdByPhoneNumber($data = [])
    {
        $record = User::where(['phone_number'  =>  $data['phone_number']])->get()->first();
        return $record;
    }

}