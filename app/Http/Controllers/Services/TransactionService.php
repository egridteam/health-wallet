<?php

namespace App\Http\Controllers\Services;

use App\Transaction;
use Illuminate\Support\Facades\DB;

class TransactionService 
{
    const TRANSACTION_TYPE = 'Credit';
    const TRANSACTION_ACTIVITY = 'Top Up';

    public static function GetMyBalance($user_id)
    {
        $record = Transaction::where([  
            'user_id' => $user_id,
            'status'  => "completed"
        ])->orderBy('id', 'DESC')->get()->first();

        $balance = (isset($record)) ? $record->balance : '0.00';
        return $balance;
    }

    public static function GetUser($user_id)
    {
        $user = Transaction::where([
            'user_id' => $user_id,
            'status'  => "completed"
        ])->orderBy('id', 'DESC')->get()->first();

        return $user;
    }

    public static function GetTransactions($data = [])
    {
        $records = Transaction::orWhere([
            'id'        =>  $data['record_id'],
            'user_id'   =>  $data['user_id'] 
        ])->orderBy('id', 'DESC')->paginate(5);

        return $records;
    }

    public static function TopUpTransaction($data = [])
    {
        DB::beginTransaction();

        try {
            $balance = CurrencyService::round_up( self::GetMyBalance($data['user_id']) );
            $evaluated_balance =  $balance + CurrencyService::round_up($data['transaction_amount']);

            // return ['eval' => $evaluated_balance, 'bal' => $balance];
            # Insert transaction to DB
            DB::table('transactions')
                ->insert([
                    'transaction_code'      => $data['transaction_code'],
                    'transaction_date'      => $data['transaction_date'],
                    'transaction_type'      => $data['transaction_type'],
                    'transaction_channel'   => $data['transaction_channel'],
                    'activity'              => $data['activity'],
                    'transaction_amount'    => $data['transaction_amount'],
                    'transaction_currency'  => $data['transaction_currency'],
                    'balance'               => $evaluated_balance,
                    'currency'              => $data['transaction_currency'],
                    'reason'                => $data['reason'],
                    'user_id'               => $data['user_id']
                ]);

            /**
             * Run Top-up Api Service
             */

            DB::commit();

            return [
                "status" => true,
                "reason" => "Transaction was successful.",
                "transaction_code"  => $data['transaction_code']
            ];

        } catch (\Exception $e) {
            DB::rollback();
            return [
                "status" => false,
                "reason" => "Error occurred while processing Top-Up."
            ];
        }
    }

    public static function TransferTransaction($receiver_id, $data = [])
    {
        DB::beginTransaction();

        $balance = CurrencyService::round_up( self::GetMyBalance($data['user_id']) );
        if ((int)$balance < $data['transaction_amount']) {
            return [
                "status" => false,
                "reason" => "Insufficient fund, please top up your wallet account."
            ];
        }

        try {
            $receiver_bal = CurrencyService::round_up( self::GetMyBalance($receiver_id) );
            $receiver_current_bal = $receiver_bal + $data['transaction_amount'];
            $evaluated_balance =  $balance - CurrencyService::round_up($data['transaction_amount']);

            # Insert transaction to DB
            DB::table('transactions')
                ->insert([
                    'transaction_code'      => $data['transaction_code'],
                    'transaction_date'      => $data['transaction_date'],
                    'transaction_type'      => $data['transaction_type'],
                    'transaction_channel'   => $data['transaction_channel'],
                    'activity'              => $data['activity'],
                    'transaction_amount'    => $data['transaction_amount'],
                    'transaction_currency'  => $data['transaction_currency'],
                    'balance'               => $evaluated_balance,
                    'currency'              => $data['transaction_currency'],
                    'reason'                => $data['reason'],
                    'user_id'               => $data['user_id']
                ]);

            # Update  transaction to DB
            $receiver = TransactionService::GetUser($receiver_id);
            DB::table('transactions')->where('id', $receiver->id)->update(['balance' => $receiver_current_bal]);

            /**
             * Run Top-up Api Service
             */

            DB::commit();

            return [
                "status" => true,
                "reason" => "Transfer was successful.",
                "transaction_code"  => $data['transaction_code']
            ];

        } catch (\Exception $e) {
            DB::rollback();
            return [
                "status" => false,
                "reason" => "Error occurred while processing Transfer."
            ];
        }
    }

    public static function LoanTransaction($data = [])
    {
        DB::beginTransaction();

        try {
            $balance = CurrencyService::round_up( self::GetMyBalance($data['user_id']) );
            $evaluated_balance =  $balance + CurrencyService::round_up($data['transaction_amount']);

            # Insert transaction to DB
            DB::table('transactions')
                ->insert([
                    'transaction_code'      => $data['transaction_code'],
                    'transaction_date'      => $data['transaction_date'],
                    'transaction_type'      => $data['transaction_type'],
                    'transaction_channel'   => $data['transaction_channel'],
                    'activity'              => $data['activity'],
                    'transaction_amount'    => $data['transaction_amount'],
                    'transaction_currency'  => $data['transaction_currency'],
                    'balance'               => $evaluated_balance,
                    'currency'              => $data['transaction_currency'],
                    'reason'                => $data['reason'],
                    'user_id'               => $data['user_id']
                ]);

            /**
             * Run Top-up Api Service
             */

            DB::commit();

            return [
                "status" => true,
                "reason" => "Loan request was successful.",
                "transaction_code"  => $data['transaction_code']
            ];

        } catch (\Exception $e) {
            DB::rollback();
            return [
                "status" => false,
                "reason" => "Error occurred while processing loan request."
            ];
        }
    }
}
