<?php

namespace App\Http\Controllers\Services;


use App\Transaction;

class LoanService
{
    const TRANSACTION_TYPE = 'Debit';
    const TRANSACTION_ACTIVITY = 'Loan';

    public static function GetLoanTransactions($data = [])
    {
        $records = Transaction::where([
            'user_id'   =>  $data['user_id'],
            'activity'  =>  self::TRANSACTION_ACTIVITY
        ])->orderBy('id', 'DESC')->paginate(5);

        return $records;
    }
}