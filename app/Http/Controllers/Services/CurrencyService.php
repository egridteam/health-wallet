<?php

namespace App\Http\Controllers\Services;

use App\Currency;

class CurrencyService 
{
    const UNIT_CURRENCY = 42;

    public static function GetAvaliableCurrencies()
    {
        $records = Currency::orWhere(['is_enabled' =>  'true'])->get();
        return $records;
    }

    public static function GetLocalCurrency() 
    {
        $record = Currency::where(['code' => 'GHC'])->get()->first();
        return $record;
    }

    public static function GetCurrencySymbol($id)
    {
        $object =  Currency::find($id);
        return $object->symbol;
    }

    public static function IsGhanaCedis($currency_id)
    {
        $record = Currency::where(['code' => 'GHC'])->get()->first();

        if(isset($records)) {
            if($currency_id ==  $record->id) {
                return true;
            }
            return false;
        }
        return false;
    }

    public static function ConvertToLocal($amount, $currency_id)
    {
        $record = Currency::find($currency_id);
        $local_cur = self::GetLocalCurrency();

        if(!isset($record))
            return ['transaction_amount' =>  self::round_up($amount, 2), 'transaction_currency' => $local_cur->id];
        
        $xrate = ($record->rate > 0) ? $record->rate : 1 ;
        $xcurrency = ($amount * 1) / $xrate;
        $xcurrency = self::round_up($xcurrency, 2);

        return ['transaction_amount' =>  $xcurrency, 'transaction_currency' => $local_cur->id];
    }

    public static function round_up ( $value, $precision = 2 ) 
    { 
        $pow = pow ( 10, $precision ); 
        return ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow; 
    } 

}
