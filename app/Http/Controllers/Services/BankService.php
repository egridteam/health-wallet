<?php

namespace App\Http\Controllers\Services;

use App\BankDetail;

class BankService 
{
    const HEALTH_WALLET = 'Health-Wallet';
    const MOBILE_SERVICE = 'Mobile-Money-Service';

    public static function GetMyBankDetails($data = [])
    {
        $records = BankDetail::orWhere([
            'id'        =>  $data['record_id'],
            'user_id'   =>  $data['user_id'] 
        ])->get();

        return $records;
    }

    public static function GetMyMobileServiceDetails($data = [])
    {
        $records = BankDetail::where([
            'user_id'   =>  $data['user_id'],
            'bank_type' =>  self::MOBILE_SERVICE
        ])->get();

        return $records;
    }

    public static function GetMyBank($data = [])
    {
        $record = BankDetail::where([
            'id'        =>  $data['record_id'],
            'user_id'   =>  $data['user_id'] 
        ])->get()->first();

        return $record;
    }

    public static function AddBankMomoDetails($data = [])
    {
        $bank_detail = new BankDetail;

        $bank_detail->user_id           = $data['user_id'];
        $bank_detail->account_number    = $data['account_number'];
        $bank_detail->account_name      = $data['account_name'];
        $bank_detail->bank_name         = $data['bank_name'];
        $bank_detail->bank_type         = $data['bank_type'];

        if( $bank_detail->save() )
            return ["status" => true, "reason" => "Bank detail added successfully."];
        else
            return ["status" => false, "reason" => "Error occurred while saving Bank Details. Please check your fields."];
    }

    public static function UpdateBankMomoDetails($data = [])
    {
        $update = BankDetail::where([
            'user_id'   =>  $data['user_id'],
            'id'        =>  $data['record_id']
        ])->update([
            'account_number'  =>  $data['account_number'],
            'account_name'    =>  $data['account_name'],
            'bank_name'       =>  $data['bank_name'],
            'bank_type'       =>  $data['bank_type']
        ]);
    }

    public static function DeleteBankMomoDetails($data = [])
    {
        try {
            $record = BankDetail::where([
                'user_id'   =>  $data['user_id'],
                'id'        =>  $data['record_id']
            ]);
            $record->delete();
            return ["status" => true, "reason" => "Record deleted successfully."];
        } catch(\Exception $e) {
            return ["status" => false, "reason" => "Error occurred while deleting bank detail record."];
        }
    }
}
