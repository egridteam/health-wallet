<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::get('/home',                     'HomeController@index')->name('home');
    Route::get('/transactions',             'Modules\Transactions@Index')->name('transactions');
    Route::get('/transfer',                 'Modules\TransferController@index')->name('transfer');
    Route::get('/bills',                    'Modules\PayBillController@index')->name('bills');
    Route::get('/loan',                     'Modules\LoanController@index')->name('loan');
    Route::get('/bank',                     'Modules\MyBankController@Index')->name('bank-index');

    
    Route::post('/bank/add',                'Modules\MyBankController@AddBankDetail')->name('add-my-bank-details');
    Route::post('/transaction/topup',       'Modules\Transactions@MakeTopUp')->name('transaction-topup');
    Route::post('/transaction/transfer',    'Modules\TransferController@MakeATransfer')->name('transaction-transfer');
    Route::post('/transaction/loan',        'Modules\LoanController@TakeALoan')->name('transaction-loan');
    Route::post('/transactions',            'Modules\SearchController@search');
});

