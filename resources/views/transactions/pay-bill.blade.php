@extends('layouts.master')

@section('title', 'Ward')

@section('content')

    @include('elements.sidebar')

    @include('elements.header')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        <div class="row header-box">
            <div class="col-md-12 breadcrumb-wrapper pd-30">
                <div class="d-flex">
                    <h4 class="tx-gray-800 mg-b-5 tx-25">Pay Bill</h4>
                </div>
                <ol class="breadcrumb">
                    <li><a href="{{ url('/home') }}" class="text-brand-color">Home</a></li>
                    <li><a href="{{ url('/transaction') }}" class="text-brand-color">Transaction</a></li>
                    <li class="active">Pay Bill</li>
                </ol>
            </div>
        </div>

        <div class="br-pagebody mg-t-5 pd-x-30 mt-5">
            <form method="post" action="{{ url('/transaction/pay-bill') }}" enctype="multipart/form-data">
                @csrf
                <div class="row pb-3 mb-3">
                    <div class="col-md-4">
                        <label class="font-weight-bold text-brand-dark">Currency</label>
                        <div class="currency-input">
                            <select class="form-control tx-12 tx-gray-600 text_field @error('currency') is-invalid @enderror"
                                    data-placeholder="Pick a currency" name="currency">
                                <option label="Pick a currency"></option>
                                @foreach ($currencies as $currency)
                                    <option value="{{ $currency->id }}">
                                        {{ $currency->country }} -  {{ $currency->code }} ({{ $currency->symbol }})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @include('elements.error', ['fieldName' => 'currency'])
                    </div>

                    <div class="col-md-4">
                        <label class="font-weight-bold text-brand-dark">Enter Amount</label>
                        <input type="text" name="amount" class="form-control @error('amount') is-invalid @enderror"
                               value="{{ old('amount') }}">
                        @include('elements.error', ['fieldName' => 'amount'])
                    </div>

                    <div class="col-md-4">
                        <label class="font-weight-bold text-brand-dark">Payment Method</label>
                        <select class="form-control" name="payment_method" id="pay-method" onchange="loadPaymentMethod()">
                            <option value>Please Choose a payment method...</option>
                            <option value="debit-card">Debit Card</option>
                            <option value="mobile-money">Mobile Money</option>
                            <option value="health-wallet">Health Wallet Account</option>
                        </select>
                        @include('elements.error', ['fieldName' => 'payment_method'])
                    </div>
                </div>

                <div class="card shadow-base bd-0 mb-5" id="debit-card">
                    <div class="card-body">
                        <h3 class="title text-brand-color text-thin mb-3">Debit Card</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Card Number</label>
                                <input type="text" class="form-control @error('card_number') is-invalid @enderror"
                                       value="{{ old('card_number') }}" name="card_number" >
                                @include('elements.error', ['fieldName' => 'card_number'])
                            </div>
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Card Name</label>
                                <input type="text" class="form-control @error('card_name') is-invalid @enderror"
                                       value="{{ old('card_name') }}" name="card_name">
                                @include('elements.error', ['fieldName' => 'card_name'])
                            </div>
                        </div>

                        <div class="row pt-2">
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Card Validity Date</label>
                                <input type="text" class="form-control @error('card_date') is-invalid @enderror"
                                       value="{{ old('card_date') }}" placeholder="MM/YY"  name="card_date">
                                @include('elements.error', ['fieldName' => 'card_date'])
                            </div>
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">CVV</label>
                                <input type="number" name="cvc" class="form-control @error('cvc') is-invalid @enderror"
                                       value="{{ old('cvc') }}">
                                @include('elements.error', ['fieldName' => 'cvc'])
                            </div>
                        </div>
                    </div><!-- card-body -->
                </div><!-- card -->

                <div class="card shadow-base bd-0 mb-5" id="mobile-money">
                    <div class="card-body">
                        <h3 class="title text-brand-color text-thin mb-3">Mobile Money</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Mobile Network</label>
                                <select class="form-control" name="network">
                                    <option value>Please Choose one</option>
                                    @foreach ($mobilebanks as $mobilebank)
                                        <option value="{{ $mobilebank->id }}">
                                            {{ $mobilebank->bank_name }}
                                        </option>
                                    @endforeach
                                </select>
                                @include('elements.error', ['fieldName' => 'network'])
                            </div>
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Phone Number</label>
                                <input type="number" class="form-control @error('phone_number') is-invalid @enderror"
                                       value="{{ old('phone_number') }}"  name="phone_number">
                                @include('elements.error', ['fieldName' => 'phone_number'])
                            </div>
                        </div>
                    </div><!-- card-body -->
                </div><!-- card -->

                <div class="card shadow-base bd-0 mb-5" id="health-wallet">
                    <div class="card-body">
                        <h3 class="title text-brand-color text-thin mb-3">Health Wallet Account</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Wallet Account Phone Number</label>
                                <input type="text" id="telephone" placeholder="Phone Number"
                                       class="form-control tx-12 text_field @error('phone_number') is-invalid @enderror"
                                       name="phone_number" value="{{ old('phone_number') }}" autocomplete="phone_number">

                                @include('elements.error', ['fieldName' => 'phone_number'])
                            </div>
                        </div>
                    </div><!-- card-body -->
                </div><!-- card -->


                <button class="btn btn-brand">
                    SUBMIT
                </button>
            </form>
        </div>

    @include('elements.footer')
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->
@endsection