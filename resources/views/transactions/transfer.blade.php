@extends('layouts.master')

@section('title', 'Ward')

@section('content')

    @include('elements.sidebar')

    @include('elements.header')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        <div class="row header-box">
            <div class="col-md-12 breadcrumb-wrapper pd-30">
                <div class="d-flex">
                    <h4 class="tx-gray-800 mg-b-5 tx-25">Transfer</h4>
                    <a class="btn btn-sm btn-outline-brand ml-3 px-3" href="#" data-toggle="modal" data-target="#modalTransfer">
                        <i class="icon ion-paper-airplane"></i> Transfer to a Wallet Account
                    </a>

                    <!-- MODAL GRID -->
                    <div id="modalTransfer" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content bd-0 bg-transparent rounded overflow-hidden">
                                <div class="modal-body pd-0">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 bg-brand-color">
                                            <div class="pd-40">
                                                <img src="{{ asset('img/transfer.svg') }}" class="w-100 pt-5">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 bg-white">
                                            <div class="pd-30">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <div class="pd-x-30 pd-y-10">
                                                    <h3 class="tx-inverse mg-b-5 text-thin">Transfer To Another Health Wallet Account</h3>
                                                    <br>
                                                    <form method="post" action="{{ route('transaction-transfer') }}" enctype="multipart/form-data">
                                                        @csrf

                                                        <div class="pd-y-6">
                                                            <label class="font-weight-bold text-brand-dark">Amount</label>
                                                            <input type="text" name="amount"
                                                                   class="form-control @error('amount') is-invalid @enderror"
                                                                   value="{{ old('amount') }}"
                                                                   placeholder="0.00">
                                                            @include('elements.error', ['fieldName' => 'amount'])
                                                        </div>

                                                        <div class="pd-y-6">
                                                            <label class="font-weight-bold text-brand-dark">
                                                                Receiver health wallet phone number
                                                            </label>
                                                            <input type="text" id="telephone" name="phone_number"
                                                                   placeholder="0900 300 ****"
                                                                   class="form-control tx-12 text_field @error('phone_number') is-invalid @enderror">
                                                            @include('elements.error', ['fieldName' => 'phone_number'])
                                                        </div>

                                                        <button class="btn btn-brand btn-block mt-3">Send</button>
                                                    </form>
                                                </div>
                                            </div><!-- pd-20 -->
                                        </div><!-- col-6 -->
                                    </div><!-- row -->
                                </div><!-- modal-body -->
                            </div><!-- modal-content -->
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                </div>

                <ol class="breadcrumb">
                    <li><a href="{{ url('/home') }}" class="text-brand-color">Home</a></li>
                    <li><a href="{{ url('/transaction') }}" class="text-brand-color">Transaction</a></li>
                    <li class="active">Transfer</li>
                </ol>
            </div>
        </div>

        <div class="br-pagebody mg-t-5 pd-x-30 mt-5">
            <form method="post" action="{{ url('/transaction/pay-bill') }}" enctype="multipart/form-data">
                @csrf
                <div class="row pb-3 mb-3">
                    <div class="col-md-6">
                        <label class="font-weight-bold text-brand-dark">Currency</label>
                        <select class="form-control tx-12 tx-gray-600 text_field @error('currency') is-invalid @enderror"
                                name="sending_currency">
                            <option label="Pick a currency"></option>
                            @foreach ($currencies as $currency)
                                <option value="{{ $currency->id }}">{{ $currency->country }} -  {{ $currency->code }} ({{ $currency->symbol }})</option>
                            @endforeach
                        </select>
                        @include('elements.error', ['fieldName' => 'currency'])
                    </div>

                    <div class="col-md-6">
                        <label class="font-weight-bold text-brand-dark">Amount</label>
                        <input type="text" name="amount" class="form-control @error('amount') is-invalid @enderror"
                               value="{{ old('amount') }}">
                        @include('elements.error', ['fieldName' => 'amount'])
                    </div>
                </div>

                <div class="card shadow-base bd-0 mb-5">
                    <div class="card-body">
                        <h3 class="title text-brand-color text-thin mb-3">Transfer To An External Account</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Account Number</label>
                                <input type="text" class="form-control @error('account_number') is-invalid @enderror"
                                       value="{{ old('account_number') }}" name="account_number" >
                                @include('elements.error', ['fieldName' => 'account_number'])
                            </div>
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Account Name</label>
                                <input type="text" class="form-control @error('account_name') is-invalid @enderror"
                                       value="{{ old('account_name') }}" name="account_name">
                                @include('elements.error', ['fieldName' => 'account_name'])
                            </div>
                        </div>

                        <div class="row pt-2">
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Bank Name</label>
                                <input type="text" class="form-control @error('bank_name') is-invalid @enderror"
                                       value="{{ old('bank_name') }}"  name="bank_name">
                                @include('elements.error', ['fieldName' => 'bank_name'])
                            </div>
                        </div>
                    </div><!-- card-body -->
                </div><!-- card -->

                <button class="btn btn-brand">
                    SUBMIT
                </button>
            </form>
        </div>

        @include('elements.footer')
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->
@endsection