@extends('layouts.master')

@section('title', 'Ward')

@section('content')

    @include('elements.sidebar')

    @include('elements.header')

    <div class="br-mainpanel">
        <div class="row header-box">
            <div class="col-md-12 breadcrumb-wrapper pd-30">
                <div class="d-flex">
                    <h4 class="tx-gray-800 mg-b-5 tx-25">Transactions</h4>
                    <a class="btn btn-sm btn-outline-brand ml-3 px-3" href="#" data-toggle="modal" data-target="#modalFund">
                        <span class="fa fa-plus"></span> Add Funds
                    </a>
                    <!-- MODAL GRID -->
                    <div id="modalFund" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content bd-0 bg-transparent rounded overflow-hidden">
                                <div class="modal-body pd-0">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 bg-brand-color">
                                            <div class="pd-40">
                                                <img src="{{ asset('img/wallet.svg') }}" class="w-100 pt-5">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 bg-white">
                                            <div class="pd-30">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <div class="pd-x-30 pd-y-10">
                                                    <h3 class="tx-inverse mg-b-5 text-thin">Top Up Your Health Wallet Account</h3>
                                                    <br>
                                                    <form method="post" action="{{ route('transaction-topup') }}" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="pd-y-6">
                                                            <div class="currency-input">
                                                                <select class="form-control tx-12 tx-gray-600
                                                                        text_field @error('currency') is-invalid @enderror"
                                                                        data-placeholder="Pick a currency"  name="sending_currency">
                                                                    <option label="Pick a currency"></option>
                                                                    @foreach ($currencies as $currency)
                                                                        <option value="{{ $currency->id }}">{{ $currency->country }} -  {{ $currency->code }} ({{ $currency->symbol }})</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            @include('elements.error', ['fieldName' => 'currency'])
                                                        </div>

                                                        <div class="pd-y-6">
                                                            <input type="text" name="amount" class="form-control @error('amount') is-invalid @enderror"
                                                                   value="{{ old('amount') }}" placeholder="Enter Amount">
                                                            @include('elements.error', ['fieldName' => 'amount'])
                                                        </div>

                                                        <div class="pd-y-6">
                                                            <select class="form-control tx-12 tx-gray-600 text_field @error('') is-invalid @enderror"
                                                                    data-placeholder="Select your Bank Account" id="" name="source">
                                                                <option label="Select your Bank Account"></option>
                                                                @foreach ($banks as $bank)
                                                                    <option value="{{ $bank->id }}">{{ $bank->bank_name }}</option>
                                                                @endforeach
                                                                
                                                            </select>
                                                            @include('elements.error', ['fieldName' => ''])
                                                        </div>

                                                        <button class="btn btn-brand btn-block mt-3">Save</button>
                                                    </form>
                                                </div>
                                            </div><!-- pd-20 -->
                                        </div><!-- col-6 -->
                                    </div><!-- row -->
                                </div><!-- modal-body -->
                            </div><!-- modal-content -->
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                </div>

                <ol class="breadcrumb">
                    <li><a href="{{ route('home') }}" class="text-brand-color">Home</a></li>
                    <li class="active">Transactions</li>
                </ol>
            </div>
        </div>

        <div class="br-pagebody mg-t-5 pd-x-30 mt-5">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <label class="font-weight-bold text-brand-dark">Filter by transaction channel: </label>
                    <form action="{{ url('/transactions') }}" method="POST">
                        @csrf
                        <div class="input-group">
                            <input type="text" class="form-control form-control-sm" name="search">
                            <span  class="input-group-prepend">
                                <button class="btn btn-sm btn-brand" type="submit">
                                    <i class="icon ion-search mr-1"></i> Search
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
      
            @if (Session::has('success'))
                @include('elements.alert', ['alert' => 'success', 'mark' => 'checkmark'])
            @elseif(Session::has('error'))
                @include('elements.alert', ['alert' => 'danger', 'mark' => 'close'])
            @endif
            @if ($errors->any())
                <div class="alert alert-danger alert-bordered pd-y-20" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="d-flex align-items-center justify-content-start">
                        <i class="icon ion-ios-close alert-icon tx-52 tx-danger mg-r-20"></i>
                        <div>
                            <span>{{ $errors->first() }}</span>
                        </div>
                    </div>
                </div>
            @endif

            <div class="d-flex align-items-center justify-content-between mg-b-30">
                <div>
                    <h6 class="tx-17 tx-uppercase tx-inverse tx-semibold tx-spacing-1">Transactions</h6>
                </div>
            </div>

            @if(sizeof($transactions) > 0)
                <div id="table_data">
                    @include('elements.data-table', ['date' => 'Transaction'])
                </div>
            @else
                <div class="text-center">
                    <div class="empty-state">
                        <i class="menu-item-icon icon ion-filing tx-100 rounded-circle px-4"></i>
                    </div>
                    <p class="tx-25">You have no transactions yet.</p>
                </div>
            @endif

            {{ $transactions->render() }}
        </div>

    @include('elements.footer')
    </div>
@endsection