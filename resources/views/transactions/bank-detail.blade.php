@extends('layouts.master')

@section('title', 'Ward')

@section('content')

    @include('elements.sidebar')

    @include('elements.header')
    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        <div class="row header-box">
            <div class="col-md-12 breadcrumb-wrapper pd-30">
                <div class="d-flex">
                    <h4 class="tx-gray-800 mg-b-5 tx-25">Bank Details / Mobile Money </h4>
                </div>
                <ol class="breadcrumb">
                    <li><a href="{{ url('/home') }}" class="text-brand-color">Home</a></li>
                    <li><a href="{{ url('/transaction') }}" class="text-brand-color">Transaction</a></li>
                    <li class="active">Bank / Mobile Money Detail</li>
                </ol>
            </div>
        </div>

    

        <div class="br-pagebody mg-t-5 pd-x-30 mt-5">

            @if (Session::has('success'))
                <div class="alert alert-success alert-bordered pd-y-20" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="d-flex align-items-center justify-content-start">
                        <i class="icon ion-ios-checkmark alert-icon tx-52 tx-success mg-r-20"></i>
                        <div>
                        <h5 class="mg-b-2 tx-success">{{ Session::get('reason') }}</h5>
                        </div>
                    </div>
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-danger alert-bordered pd-y-20" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="d-flex align-items-center justify-content-start">
                        <i class="icon ion-ios-close alert-icon tx-52 tx-danger mg-r-20"></i>
                        <div>
                        <h5 class="mg-b-2 tx-danger">{{ Session::get('reason') }}</h5>
                        </div>
                    </div>
                </div>
            @endif

            <form method="post" action="{{ route('add-my-bank-details') }}" enctype="multipart/form-data">
                @csrf
                <div class="card shadow-base bd-0 mb-5">
                    <div class="card-body">
                        <h3 class="title text-brand-color text-thin mb-3">Bank / Mobile Money Details</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Select Bank Type</label>
                                <select class="form-control @error('bank_type') is-invalid @enderror"
                                        name="bank_type" >
                                    <option value>Please choose one</option>
                                    <option value="Mobile-Money-Service">Mobile-Money Service</option>
                                    <option value="Bank-Service">Bank Service</option>
                                </select>
                                @include('elements.error', ['fieldName' => 'bank_type'])
                            </div>
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Select Bank / M.Money Provider</label>
                                <select class="form-control @error('bank_name') is-invalid @enderror"
                                        name="bank_name" >
                                    <option value>Please choose one</option>
                                    <option value="MTN-Mobile-Money">MTN Mobile-Money</option>
                                    <option value="Airtel-Money">Airtel Money</option>
                                    <option value="Tigo-Cash">Tigo Cash</option>
                                    <option value="Fidelity-Bank">Fidelity Bank</option>
                                    <option value="Stanbic-Bank">Stanbic Bank</option>
                                    <option value="EcoBank">EcoBank</option>
                                    <option value="GCBank">GCBank</option>
                                </select>
                                @include('elements.error', ['fieldName' => 'bank_name'])
                            </div>
                            
                        </div>
                        
                        <div class="row pt-2 mt-10 mb-3" style="margin-top:10px;">
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Account Name</label>
                                <input type="text" class="form-control @error('account_name') is-invalid @enderror"
                                        value="{{ old('account_name') }}" name="account_name">
                                @include('elements.error', ['fieldName' => 'account_name'])
                            </div>
                            <div class="col-md-6">
                                <label class="font-weight-bold text-brand-dark">Account Number</label>
                                <input type="text" class="form-control @error('account_number') is-invalid @enderror"
                                        value="{{ old('account_number') }}" name="account_number" >
                                @include('elements.error', ['fieldName' => 'account_number'])
                            </div>
                        </div>

                        <div class="text-center">
                            <button class="btn btn-brand px-5">Add Bank details</button>
                        </div>
                    </div><!-- card-body -->
                </div><!-- card -->
            </form>
        </div>

        <div class="br-pagebody mg-t-5 pd-x-30 mt-5" style="margin-bottom:50px">
            <div class="row">
                @foreach ($banks as $bank)
                    <div class="card zoom-card" style="width:200px;margin:5px;">
                        <div class="card-body">
                            <p class="card-text">
                                <strong>{{ $bank->account_number }}</strong>
                            </p>
                            <p class="card-text text-brand-color ">
                                <small>{{ $bank->account_name }}</small>
                            </p>
                        </div>
                         
                        <img class="card-img-bottom img-fluid"  src="{{ asset('banks/'.$bank->bank_name.'.jpg') }}" alt="Image">
                       
                    </div>
                @endforeach
            </div>
        </div>


        @include('elements.footer')
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->
@endsection