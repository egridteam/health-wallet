@extends('layouts.master')

@section('title', 'Ward')

@section('content')

    @include('elements.sidebar')

    @include('elements.header')
    <div class="br-mainpanel">
        <div class="row header-box">
            <div class="col-md-12 breadcrumb-wrapper pd-30">
                <div class="d-flex">
                    <h4 class="tx-gray-800 mg-b-5 tx-25">Loan Request</h4>
                    <a class="btn btn-sm btn-outline-brand ml-3 px-3" href="#" data-toggle="modal" data-target="#modalLoan">
                        Request A Loan
                    </a>

                    <!-- MODAL GRID -->
                    <div id="modalLoan" class="modal fade">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content bd-0 bg-transparent rounded overflow-hidden">
                                <div class="modal-body pd-0">
                                    <div class="row no-gutters">
                                        <div class="col-lg-6 bg-brand-color">
                                            <div class="pd-40">
                                                <img src="{{ asset('img/loan.svg') }}" class="w-100 pt-5">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 bg-white">
                                            <div class="pd-30">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <div class="pd-x-30 pd-y-10">
                                                    <h3 class="tx-inverse mg-b-5 text-thin">
                                                        Top Your Account By Requesting For A Loan
                                                    </h3>
                                                    <br>
                                                    <form method="post" action="{{ route('transaction-loan') }}" enctype="multipart/form-data">
                                                        @csrf

                                                        <div class="pd-y-6">
                                                            <label class="font-weight-bold text-brand-dark">Amount</label>
                                                            <input type="text" name="amount"
                                                                   class="form-control @error('amount') is-invalid @enderror"
                                                                   value="{{ old('amount') }}"
                                                                   placeholder="0.00">
                                                            @include('elements.error', ['fieldName' => 'amount'])
                                                        </div>

                                                        <button class="btn btn-brand btn-block mt-3">Request Loan</button>
                                                    </form>
                                                </div>
                                            </div><!-- pd-20 -->
                                        </div><!-- col-6 -->
                                    </div><!-- row -->
                                </div><!-- modal-body -->
                            </div><!-- modal-content -->
                        </div><!-- modal-dialog -->
                    </div><!-- modal -->
                </div>
                <ol class="breadcrumb">
                    <li><a href="{{ url('/home') }}" class="text-brand-color">Home</a></li>
                    <li><a href="{{ url('/transaction') }}" class="text-brand-color">Transaction</a></li>
                    <li class="active">Loan</li>
                </ol>
            </div>
        </div>

        <div class="br-pagebody mg-t-5 pd-x-30 mt-5">
            <div class="d-flex align-items-center justify-content-between mg-b-30">
                <div>
                    <h6 class="tx-17 tx-uppercase tx-inverse tx-semibold tx-spacing-1">List of Loan Request</h6>
                </div>
            </div>

            @if(sizeof($transactions) > 0)
                @include('elements.data-table', ['date' => 'Request'])
            @else
                <div class="text-center">
                    <div class="empty-state">
                        <i class="menu-item-icon icon ion-cash tx-100 rounded-circle px-4"></i>
                    </div>
                    <p class="tx-25">You have not request for loan yet.</p>
                </div>
            @endif

            {{ $transactions->links() }}
        </div>

    @include('elements.footer')
    </div>
@endsection