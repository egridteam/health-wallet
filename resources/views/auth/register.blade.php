@extends('layouts.master')

@section('title', 'Register')

@section('content')
<div class="align-items-center justify-content-center login-bg ht-100v">
    <form method="post" action="{{ route('register') }}" id="regForm">
        @csrf

        <div class="mx-auto signin-logo login-wrapper wd-300 wd-xs-400 pd-25 m tx-center">
            {{--<img src="{{ asset('img/logo.jpg') }}" width="150" height="60">--}}
            <h3 class="text-thin text-white">Sign Up</h3>
        </div>
        <div class=" mx-auto login-wrapper wd-300 wd-xs-400 pd-25 bg-white rounded shadow-base">

            <div class="tab">
                <div class=" tx-center tx-28 tx-bold tx-inverse pb-3">
                    <h3 class="title text-brand-color float-left text-thin mb-3">Personal Information</h3>
                </div>
                <div class="form-group">
                    <input id="first_name" type="text" placeholder="First Name"
                           class="form-control tx-12 text_field @error('first_name') is-invalid @enderror"
                           name="first_name" value="{{ old('first_name') }}" autocomplete="first_name" autofocus>

                    @include('elements.error', ['fieldName' => 'first_name'])
                    <span class="error-msg text-danger tx-12">this field is required</span>
                </div>

                <div class="form-group">
                    <input id="last_name" type="text" placeholder="Last Name"
                           class="form-control tx-12 text_field @error('last_name') is-invalid @enderror"
                           name="last_name" value="{{ old('last_name') }}" autocomplete="last_name" autofocus>

                    @include('elements.error', ['fieldName' => 'last_name'])
                    <span class="error-msg text-danger tx-12">this field is required</span>
                </div>

                <div class="form-group">
                    <select class="form-control tx-12 tx-gray-600 text_field @error('country') is-invalid @enderror"
                            data-placeholder="Pick a country" id="country" name="country">
                        <option label="Pick a country"></option>
                    </select>
                    @include('elements.error', ['fieldName' => 'country'])
                    <span class="error-msg text-danger tx-12">this field is required</span>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <input class="form-control tx-12 fc-datepicker text_field @error('date_of_birth') is-invalid @enderror"
                               type="text" placeholder="Date of Birth" name="date_of_birth" data-toggle="datepicker">
                        <span id="datepicker" class="input-group-addon"><i class="icon ion-calendar"></i></span>
                    </div>
                    @include('elements.error', ['fieldName' => 'date_of_birth'])
                    <span class="error-msg text-danger tx-12">this field is required</span>
                </div>
            </div>

            <div class="tab">
                <div class=" tx-center tx-28 tx-bold tx-inverse pb-3">
                    <h3 class="title text-brand-color float-left text-thin mb-3">Contact Information</h3>
                </div>

                <div class="form-group">
                    <input id="email" type="email" placeholder="Email Address"
                           class="form-control tx-12 text_field @error('email') is-invalid @enderror"
                           name="email" value="{{ old('email') }}" autocomplete="email">

                    @include('elements.error', ['fieldName' => 'email'])
                    <span class="error-msg text-danger tx-12">this field is required</span>
                </div>

                <div class="form-group">
                    <input type="text" id="telephone" placeholder="Phone Number"
                           class="form-control tx-12 text_field @error('phone_number') is-invalid @enderror"
                           name="phone_number" value="{{ old('phone_number') }}" autocomplete="phone_number">

                    @include('elements.error', ['fieldName' => 'phone_number'])
                    <span class="error-msg text-danger tx-12">this field is required</span>
                </div>

                <div class="form-group">
                    <input id="password" type="password" placeholder="Password"
                           class="form-control tx-12 text_field @error('password') is-invalid @enderror"
                           name="password" autocomplete="new-password">

                    @include('elements.error', ['fieldName' => 'password'])
                    <span class="error-msg text-danger tx-12">this field is required</span>
                </div>

                <div class="form-group">
                    <input id="password-confirm" type="password"
                           placeholder="Confirm Password"
                           class="form-control tx-12 text_field" name="password_confirmation"
                           autocomplete="new-password">
                    <span class="error-msg text-danger tx-12">this field is required</span>
                </div>
            </div>

            <div style="overflow:auto;">
                <div class="float-right">
                    <button type="button" id="prevBtn" onclick="nextPrev(-1)" class="btn btn-brand btn-sm px-4">Back</button>
                    <button type="button" id="nextBtn" onclick="nextPrev(1)" class="btn btn-brand btn-sm px-3">Next</button>
                </div>
            </div>

            <div style="text-align:center;margin-top:40px;">
                <span class="step"></span>
                <span class="step"></span>
            </div>

            <div class="mg-t-40 tx-center">
                Already a member? <a href="{{ url('login') }}" class="text-brand-color sign">Login</a>
            </div>
        </div>

    </form>
</div>

@endsection
