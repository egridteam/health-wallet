@extends('layouts.master')

@section('title', 'Forget Password')

@section('content')

<form method="POST" action="{{ route('password.email') }}">
    @csrf
    <div class="d-flex align-items-center justify-content-center login-bg ht-100v">

        <div class="login-wrapper wd-300 wd-xs-350 pd-25 bg-white rounded shadow-base">
            <div class="signin-logo tx-center tx-28 tx-bold tx-inverse">
                <img src="{{ asset('img/logo.jpg') }}" width="150" height="60">
            </div>

            @if (session()->has('status'))
                <div class="alert alert-success text-center animated fadeIn">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>
                        {!! session()->get('status') !!}
                    </strong>
                </div>
            @endif

            <div class="form-group">
                <input id="email" type="text" placeholder="Enter Email/ Phone Number"
                       class="form-control @error('email') is-invalid @enderror"
                       name="email" value="{{ old('email') }}" autofocus>

                @include('elements.error', ['fieldName' => 'email'])
            </div>

            <button type="submit" class="btn btn-brand btn-block">Send Password Reset Link</button>

            <div class="mg-t-60 tx-center"><a href="{{ url('login') }}" class="text-brand-color sign"> Back to Login</a></div>
        </div>
    </div>

</form>
@endsection