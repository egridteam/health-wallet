<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Meta -->
    <meta name="description" content="Wallet system for Health service provision.">
    <meta name="author" content="HEALTH DIRECT">

    @guest
        <title> @yield('title') </title>
    @else
        <title> @yield('title') | {{ Auth::user()->name }}</title>
@endguest

<!-- Scripts -->
    <script src="{{ asset('js/app2.js') }}" defer></script>
{{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

<!-- vendor css -->
    <link href="{{ asset('lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/jquery-switchbutton/jquery.switchButton.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/rickshaw/rickshaw.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/chartist/chartist.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/datatables/jquery.dataTables.css') }}" rel="stylesheet">

    <!-- Bracket CSS -->
    <link rel="stylesheet" href="{{ asset('css/temp_css/bracket.css') }}">
    <link rel="stylesheet" href="{{ asset('intl-tel-input-master/build/css/intlTelInput.css') }}">
    <link rel="stylesheet" href="{{ asset('datepicker-master/dist/datepicker.min.css') }}">

    <!-- Styles -->
    <link href="{{ asset('css/app2.css') }}" rel="stylesheet">
    <link href="{{ asset('css/currency-picker/bootstrap-formhelpers.min.css') }}" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>

<body>

<main class="index">
    @yield('content')
</main>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>

{{-- <script src="{{ asset('lib/jquery/jquery.js') }}"></script> --}}
<script src="{{ asset('lib/popper.js/popper.js') }}"></script>
<script src="{{ asset('lib/bootstrap/bootstrap.js') }}"></script>
<script src="{{ asset('lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
<script src="{{ asset('lib/moment/moment.js') }}"></script>
<script src="{{ asset('lib/jquery-ui/jquery-ui.js') }}"></script>
<script src="{{ asset('lib/jquery-switchbutton/jquery.switchButton.js') }}"></script>
<script src="{{ asset('lib/peity/jquery.peity.js') }}"></script>
<script src="{{ asset('lib/chartist/chartist.js') }}"></script>
<script src="{{ asset('lib/jquery.sparkline.bower/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('lib/d3/d3.js') }}"></script>
<script src="{{ asset('lib/jt.timepicker/jquery.timepicker.js') }}"></script>
<script src="{{ asset('lib/jquery.maskedinput/jquery.maskedinput.js') }}"></script>
<script src="{{ asset('lib/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script src="{{ asset('lib/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('lib/highlightjs/highlight.pack.js') }}"></script>

<!-- Use as a jQuery plugin -->
<script src="{{ asset('intl-tel-input-master/build/js/intlTelInput-jquery.min.js') }}"></script>
<script src="{{ asset('intl-tel-input-master/build/js/intlTelInput.min.js') }}"></script>
<script src="{{ asset('datepicker-master/dist/datepicker.min.js') }}"></script>

<script src="{{ asset('js/temp_js/bracket.js') }}"></script>
<script src="{{ asset('js/temp_js/ResizeSensor.js') }}"></script>
<script src="{{ asset('js/temp_js/dashboard.js') }}"></script>
<script src="{{ asset('js/currency-picker/bootstrap-formhelpers.js') }}"></script>
<script src="{{ asset('js/currency-picker/init.js') }}"></script>


<script>
    $(function(){
        'use strict'

        // FOR DEMO ONLY
        // menu collapsed by default during first page load or refresh with screen
        // having a size between 992px and 1299px. This is intended on this page only
        // for better viewing of widgets demo.
        $(window).resize(function(){
            minimizeMenu();
        });

        minimizeMenu();

        function minimizeMenu() {
            if(window.matchMedia('(min-width: 992px)').matches && window.matchMedia('(max-width: 1299px)').matches) {
                // show only the icons and hide left menu label by default
                $('.menu-item-label,.menu-item-arrow').addClass('op-lg-0-force d-lg-none');
                $('body').addClass('collapsed-menu');
                $('.show-sub + .br-menu-sub').slideUp();
            } else if(window.matchMedia('(min-width: 1300px)').matches && !$('body').hasClass('collapsed-menu')) {
                $('.menu-item-label,.menu-item-arrow').removeClass('op-lg-0-force d-lg-none');
                $('body').removeClass('collapsed-menu');
                $('.show-sub + .br-menu-sub').slideDown();
            }
        }
    });

    $(function(){
        'use strict';

        $('.select2').select2({
            minimumResultsForSearch: Infinity
        });
    });
</script>
<script>
    $('#datatable2').DataTable({
        bLengthChange: false,
        searching: false,
        bPaginate: false,
        responsive: true,
        bInfo: false
    });

    $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });

    var input = document.querySelector('#telephone');
    window.intlTelInput(input,({
        // options here
    }));
</script>

</body>
</html>