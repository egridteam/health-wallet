<footer class="main-footer">
    <strong>Copyright &copy; <?= date('Y'); ?>  <span class="text-brand-color"> Health Direct Global</span>. All Rights Reserved.</strong>
</footer>