<!-- ########## START: LEFT PANEL ########## -->
<div class="br-logo align-content-center justify-content-center">
    <a href="">
        <img src="{{ asset('img/logo.jpg') }}" width="150" height="60">
    </a>
</div>
<div class="br-sideleft overflow-y-auto">
    <label class="sidebar-label pd-x-15 mg-t-20">Navigation</label>
    <div class="br-sideleft-menu">
        <a href="{{ route('home') }}" class="br-menu-link {{ (Route::current()->getName() == 'home') ? 'active' : '' }}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Dashboard</span>
            </div>
        </a>

        <a href="{{ route('bank-index') }}" class="br-menu-link {{ (Route::current()->getName() == 'bank-index') ? 'active' : '' }}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-briefcase tx-20"></i>
                <span class="menu-item-label">My Bank Details</span>
            </div>
        </a>

        <a href="{{ route('transactions') }}" class="br-menu-link {{ (Route::current()->getName() == 'transactions') ? 'active' : '' }}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-filing tx-20"></i>
                <span class="menu-item-label">My Transaction</span>
            </div>
        </a>

        <a href="{{ route('transfer') }}" class="br-menu-link {{ (Route::current()->getName() == 'transfer') ? 'active' : '' }}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-paper-airplane tx-20"></i>
                <span class="menu-item-label">Transfer</span>
            </div>
        </a>

        <a href="{{ route('loan') }}" class="br-menu-link {{ (Route::current()->getName() == 'loan') ? 'active' : '' }}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-cash tx-20"></i>
                <span class="menu-item-label">Loan</span>
            </div>
        </a>

        <a href="{{ route('bills') }}" class="br-menu-link {{ (Route::current()->getName() == 'bills') ? 'active' : '' }}">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-card tx-20"></i>
                <span class="menu-item-label">Pay Bill</span>
            </div>
        </a>

        <a href="#" class="br-menu-link">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-gear-outline tx-20"></i>
                <span class="menu-item-label">Settings</span>
                <i class="menu-item-arrow fa fa-angle-down"></i>
            </div>
        </a>
        <ul class="br-menu-sub nav flex-column">
            <li class="nav-item"><a href="#" class="nav-link">Profile</a></li>
        </ul>

        <a class="br-menu-link" href="{{ route('logout') }}"
           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-power tx-18"></i>
                <span class="menu-item-label">Logout</span>
            </div>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>

    </div>

    <br>
</div>
<!-- ########## END: LEFT PANEL ########## -->
