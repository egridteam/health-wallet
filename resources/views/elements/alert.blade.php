<div class="alert alert-{{ $alert }} alert-bordered pd-y-20" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="d-flex align-items-center justify-content-start">
        <i class="icon ion-ios-{{ $mark }} alert-icon tx-52 tx-{{ $alert }} mg-r-20"></i>
        <div>
            <h5 class="mg-b-2 tx-{{ $alert }}">{{ Session::get('reason') }}</h5>
        </div>
    </div>
</div>