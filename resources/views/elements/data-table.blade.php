<table id="datatable2" class="table table-bordered table-striped cell-border">
    <thead>
    <tr>
        <th style="border-right:1px solid #ddd">S/N</th>
        <th style="border-right:1px solid #ddd">{{ $date }} Date</th>
        <th style="border-right:1px solid #ddd">Activity</th>
        <th style="border-right:1px solid #ddd">Channel</th>
        <th style="border-right:1px solid #ddd">Amount</th>
        <th style="border-right:1px solid #ddd">Balance</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($transactions as $key => $trx)
        <tr>
            <td>{{$key+1}}</td>
            <td class="pd-l-0-force" >
                <h6 class="tx-inverse tx-14" style="margin-left:10px;">
                    {{ $trx->transactionDate() }}
                    <span class="tx-11" style="color:#5058b8;font-weight:bold"></span>
                </h6>
                <span class="tx-11" style="color:#cb7435;margin-left:10px;">{{ $trx->transaction_code }}</span>
            </td>
            <td>
                <h6 class="tx-inverse tx-14">{{ $trx->activity }}</h6>
                <span class="tx-11" style="color:#cb7435;">{{ $trx->transaction_type }}</span>
            </td>
            <td>{{ $trx->transaction_channel }}</td>
            <td>
                <span class="text-brand-color">
                    {{ $trx->transactionCurrency()}} {{ $trx->transaction_amount }}
                </span>
            </td>
            <td>
                <span class="text-brand-color">
                    {{ $trx->currency()}} {{ $trx->balance }}
                </span>
            </td>
            <td><span class="status status-{{$trx->status}}">{{ $trx->status}}</span></td>
        </tr>
    @endforeach
    </tbody>
</table>