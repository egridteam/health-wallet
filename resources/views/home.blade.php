@extends('layouts.master')

@section('title', 'Dashboard')

@section('content')

    @include('elements.sidebar')

    @include('elements.header')

    <!-- ########## START: MAIN PANEL ########## -->
    <div class="br-mainpanel">
        <div class="pd-30 header-box">
            <h4 class="tx-gray-800 mg-b-5">Dashboard</h4>
        </div>

        <div class="br-pagebody mg-t-5 pd-x-30 mt-5">
            <div class="row row-sm">
                <div class="col-sm-6 col-xl-3 col-md-3 mg-t-20 mg-xl-t-0 pb-3">
                    <div class="bg-brand-color rounded overflow-hidden zoom-card card">
                        <div class="pd-25 d-flex align-items-center">
                            <a href="{{ url('/transactions') }}"><i class="ion ion-clipboard tx-50 tx-white"></i></a>
                            <div class="mg-l-20">
                                <a href="{{ url('/transactions') }}">
                                    <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Balance</p>
                                </a>
                                <span class="text-white tx-30">GH¢ {{$balance}}</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xl-3 col-md-3 mg-t-20 mg-xl-t-0 pb-3">
                    <div class="bg-brand-dark rounded overflow-hidden zoom-card card">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="ion ion-paper-airplane tx-50 tx-white"></i>
                            <div class="mg-l-20">
                                <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Transfers</p>
                                <span class="text-white tx-30">GH¢ 0.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3 col-md-3 mg-t-20 mg-xl-t-0 pb-3">
                    <div class="bg-orange rounded overflow-hidden zoom-card card">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="ion ion-cash tx-50 tx-white"></i>
                            <div class="mg-l-20">
                                <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Loans</p>
                                <span class="text-white tx-30">GH¢ 0.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3 col-md-3 mg-t-20 mg-xl-t-0 pb-3">
                    <div class="bg-info rounded overflow-hidden zoom-card card">
                        <div class="pd-25 d-flex align-items-center">
                            <i class="ion ion-card tx-50 tx-white"></i>
                            <div class="mg-l-20">
                                <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Bills</p>
                                <span class="text-white tx-30">GH¢ 0.00</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-6 mb-3">
                    <div class="card shadow-base bd-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-4">
                                    <img src="{{ asset('img/scan.png') }}" width="60%">
                                </div>
                                <div class="col-8">
                                    <h6 class="text-brand-dark text-align-left">
                                        <label>Pay Bill with QR Code</label>
                                        <i class="ion ion-ios-arrow-thin-right tx-30 pl-2 text-brand-color"></i>
                                    </h6>
                                    <div class="text-align-left text-brand-color tx-11">
                                        Pay in just 3 simple steps. <br> 1. Scan <br>2. Input Amount <br> 3. Confirm payment
                                    </div>
                                </div>
                            </div>
                        </div><!-- card-body -->
                    </div><!-- card -->
                </div>

                <div class="col-sm-6 col-md-3 mb-3">
                    <div class="card zoom-card shadow-base bd-0">
                        <div class="card-body bg-brand-dark">
                            <div class="row">
                                <div class="col-5">
                                    <i class="ion ion-paper-airplane tx-50 tx-white"></i>
                                </div>
                                <div class="col-7">
                                    <h5 class="tx-white text-align-left"><label>Transfer Money</label></h5>
                                </div>
                            </div>
                            <div class="tx-white tx-12 text-align-left">
                                Transfer money to another Health Direct member account.
                            </div>
                        </div><!-- card-body -->
                    </div><!-- card -->
                </div>

                <div class="col-sm-6 col-md-3 mb-3">
                    <div class="card zoom-card shadow-base bd-0">
                        <div class="card-body bg-brand-color">
                            <div class="row">
                                <div class="col-5">
                                    <i class="ion ion-ios-people-outline tx-50 tx-white"></i>
                                </div>
                                <div class="col-7">
                                    <h5 class="tx-white text-align-left"><label>Crowd Funding</label></h5>
                                </div>
                            </div>
                            <div class="tx-white tx-12 text-align-left">
                                Start a Crowd Funding Campaign to request fund from your friends
                            </div>
                        </div><!-- card-body -->
                    </div><!-- card -->
                </div>
            </div>


        </div>
        <!--br-pagebody-->
            @include('elements.footer')
    </div><!-- br-mainpanel -->
    <!-- ########## END: MAIN PANEL ########## -->
@endsection
