@extends('layouts.index')

@section('title', 'home page')

@section('content')
    <div class="header-bg">
        <div class="navbar-logo-header fixed-top">
            <div class="container">
                <div class="navbar-brand">
                    <img src="{{ asset('img/logo.jpg') }}" width="150" height="60">
                </div>
                <div class="sign-btn" style="float:right; margin-top:10px">
                    <a href="{{ url('login') }}" class="btn btn-brand btn-sm px-3 tx-12">Sign In</a>
                    <a href="{{ url('register') }}" class="btn btn-brand btn-sm px-3 tx-12">Sign Up</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="container">
                <div class="col-md-6 mx-auto text-center text-white header-text">
                    <label class="font-weight-bold tx-30">DO IT ON HEALTH DIRECT GlOBAL</label>
                    <p class="mt-3 lead">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                        ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    </p>
                    <a href="{{ url('login') }}" class="btn btn-white mt-5 header-btn">Get Started</a>
                </div>
            </div>
        </div>
    </div>

    <section id="about">
        <div class="container text-center">
            <h1><label>About Us</label></h1>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                mollit anim id est laborum.
            </p>
        </div>
    </section>

    <section id="works">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 mb-3">
                    <div class="card card-width text-center zoom-card">
                        <div class="card-body">
                            <img src="{{ asset('img/smart.png') }}" class="w-75"/>
                            <h5 class="text-brand-dark mt-5 mb-3">
                                <label>Smart Savings, Healthy Living</label>
                            </h5>
                            <p class="pb-2 text-gray lead">
                                Your life is precious, make it count. Timely access to financial
                                assistance for all your medical needs.
                            </p>
                        </div><!-- card-body -->
                    </div><!-- card -->
                </div>

                <div class="col-sm-6 col-md-4 mb-3">
                    <div class="card card-width text-center zoom-card">
                        <div class="card-body">
                            <img src="{{ asset('img/walking.png') }}" class="w-75"/>
                            <h5 class="text-brand-dark mt-5 mb-3">
                                <label>Inclusive health for all</label>
                            </h5>
                            <p class="pb-2 lead text-gray">
                                Timely, quality, affordable health care where and when you need it.
                            </p>
                        </div><!-- card-body -->
                    </div><!-- card -->
                </div>

                <div class="col-sm-6 col-md-4 mb-3">
                    <div class="card card-width text-center zoom-card">
                        <div class="card-body">
                            <img src="{{ asset('img/secure.png') }}" class="w-75"/>
                            <h5 class="text-brand-dark mt-5 mb-3">
                                <label>Secure Health Wallet</label>
                            </h5>
                            <p class="pb-2 lead text-gray">
                                Create a free health savings account to pay for all your medical needs.
                            </p>
                        </div><!-- card-body -->
                    </div><!-- card -->
                </div>

            </div>
        </div>
    </section>

    <section id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-2 quick-links">
                    <h6><label>Quick Links</label></h6>
                    <p><a href="#about"><span class="text-gray tx-12">About Us</span></a></p>
                    <p><a href="#works"><span class="text-gray tx-12">FAQ</span></a></p>
                    <p>
                        <a href="" data-toggle="modal" data-target="#policyModal">
                            <span class="text-gray tx-12">Privacy policy</span>
                        </a>
                        @include('elements.modal', ['modalId' => 'policyModal', 'modalTitle' => 'Privacy Policy'])
                    </p>
                    <p>
                        <a href="" data-toggle="modal" data-target="#tcModal">
                            <span class="text-gray tx-12">Terms &amp; Conditions</span>
                        </a>
                        @include('elements.modal', ['modalId' => 'tcModal', 'modalTitle' => 'Terms & Conditions'])
                    </p>
                </div>

                <div class="col-sm-2">
                    <h6><label>Contact</label></h6>
                    <p class="text-gray tx-12">+234 (0) 703 710 3288</p>
                    <p class="email text-gray tx-12">info@heathdirect.com</p>
                </div>

                <div class="col-sm-2">
                    <h6><label>Address</label></h6>
                    <p class="text-gray">123 Main Street, Lekki, Lagos, Nigeria</p>
                </div>

                <div class="col-sm-2">
                    <h6><label>Transactions</label></h6>
                    <p class="text-gray tx-12">Transfer</p>
                    <p class="text-gray tx-12">Top Up</p>
                    <p class="text-gray tx-12">Request Loan</p>
                    <p class="text-gray tx-12">Make a Payment</p>
                    <p class="text-gray tx-12">Pay Bills</p>
                </div>

                <div class="col-sm-4">
                    <h6><label>What We Do!</label></h6>
                    <p class="text-gray tx-12">
                        Health Direct Global helps you start a Crowd Funding Campaign to request fund from your friends
                    </p>
                    <p class="social-media-logo-container">
                        <a class="btn btn-brand-media mr-2" href="#" target="_blank">
                            <i class="fa fa-linkedin"></i>
                        </a>
                        <a class="btn btn-brand-media mr-2" href="#" target="_blank">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a class="btn btn-brand-media mr-2" href="#" target="_blank">
                            <i class="fa fa-twitter text-white-color"></i>
                        </a>
                    </p>
                </div>
            </div>

            <div class="copyright-footer">
                <p class="text-center text-gray tx-12">
                    <span>© Copyright <?= date('Y'); ?> Health Direct Global. All rights reserved. </span>
                    <a class="text-brand-color" href="#">www.healthdirect.com</a>
                </p>
            </div>
        </div>
    </section>

@endsection
