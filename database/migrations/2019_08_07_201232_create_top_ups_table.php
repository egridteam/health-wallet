<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopUpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('top_ups', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('amount');
            $table->string('currency');
            $table->unsignedInteger('bank_detail_id');
            $table->foreign('bank_detail_id')->references('id')->on('bank_details')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('top_ups', function (Blueprint $table) {
            $table->dropForeign('top_ups_bank_detail_id_foreign');
        });
        Schema::dropIfExists('top_ups');
    }
}
