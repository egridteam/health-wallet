<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_code');
            $table->string('transaction_date');
            $table->string('transaction_type'); # Credit | Debit | 
            $table->string('transaction_channel'); # Momo Pay | Bank | Local
            $table->string('activity'); # Top-Up | Pay-Bill | Transfer | 
            $table->string('transaction_amount')->default("0.00");
            $table->string('transaction_currency'); # USD | NGN | EURO
            $table->string('balance')->default("0.00");
            $table->string('currency'); # Def: Ghanian Cedi
            $table->string('reason')->nullable();
            $table->string('status')->default('pending'); # Pending | Cancelled | Completed
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign('transactions_user_id_foreign');
        });
        Schema::dropIfExists('transactions');
    }
}
