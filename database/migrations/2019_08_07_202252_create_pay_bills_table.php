<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_bills', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('amount');
            $table->string('currency');
            $table->string('payment_method');
            $table->string('card_number')->nullable();
            $table->string('card_name')->nullable();
            $table->string('card_valid_date')->nullable();
            $table->string('cvv')->nullable();
            $table->string('mobile_network')->nullable();
            $table->string('phone_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pay_bills', function (Blueprint $table) {
            $table->dropForeign('pay_bills_user_id_foreign');
        });
        Schema::dropIfExists('pay_bills');
    }
}
